package co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Repository;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.com.ceiba.mobile.pruebadeingreso.App.App;
import co.com.ceiba.mobile.pruebadeingreso.BuildConfig;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Interfaces.IListenerResponseRepositoryPost;
import co.com.ceiba.mobile.pruebadeingreso.Models.Post;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.Utils.Constants;
import co.com.ceiba.mobile.pruebadeingreso.Volley.VolleyQueueManager;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class RepositoryPost {

    public void callServiceGetPostUser(Integer id, final IListenerResponseRepositoryPost handler) {
        String url = BuildConfig.URL_API_BASE + Constants.GET_POST_USER + id;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Type postType = new TypeToken<List<Post>>() {
                        }.getType();
                        List<Post> postList = new Gson().fromJson(String.valueOf(response), postType);
                        handler.onApiGetPostUsersReponseSuccess(postList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handler.onApiGetPostUsersReponseError(App.getInstance().getContext().getString(R.string.message_get_post_users_response_error));
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleyQueueManager.getInstance(App.getInstance().getContext()).addToRequestQueue(request);

    }


}
