package co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.BussinessLogic;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.App.App;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Interfaces.IListenerResponseBussinessLogicPost;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Interfaces.IListenerResponseRepositoryPost;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Repository.RepositoryPost;
import co.com.ceiba.mobile.pruebadeingreso.Models.Post;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.Utils.ServiceNetwork;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class BussinessLogicPost implements IListenerResponseRepositoryPost {

    private IListenerResponseBussinessLogicPost iListenerResponseBussinessLogicPost;
    private RepositoryPost repositoryPost;

    public BussinessLogicPost(IListenerResponseBussinessLogicPost iListenerResponseBussinessLogicPost) {
        this.iListenerResponseBussinessLogicPost = iListenerResponseBussinessLogicPost;
        this.repositoryPost = new RepositoryPost();
    }

    public void getPostUser(Integer id) {
        if (ServiceNetwork.checkNetworkConnection(App.getInstance().getContext())) {
            repositoryPost.callServiceGetPostUser(id, BussinessLogicPost.this);
        } else {
            iListenerResponseBussinessLogicPost.onDeviceDontHaveNetworkConnection(App.getInstance().getString(R.string.message_error_connection_network));
        }
    }

    @Override
    public void onApiGetPostUsersReponseSuccess(List<Post> postList) {
        iListenerResponseBussinessLogicPost.onApiGetPostUsersReponseSuccess(postList);
    }

    @Override
    public void onApiGetPostUsersReponseError(String message) {
        iListenerResponseBussinessLogicPost.onApiGetPostUsersReponseError(message);
    }
}
