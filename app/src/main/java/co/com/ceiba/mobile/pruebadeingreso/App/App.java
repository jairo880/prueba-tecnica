package co.com.ceiba.mobile.pruebadeingreso.App;

import android.app.Application;
import android.content.Context;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class App extends Application {

    static App instance;

    private Context context;

    public static App getInstance() {
        if (instance == null) {
            instance = new App();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}
