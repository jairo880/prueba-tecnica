package co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Presenter;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.BussinessLogic.BussinessLogicHome;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces.IListenerResponseBussinessLogicHome;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces.IPresenterHome;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces.IViewHome;
import co.com.ceiba.mobile.pruebadeingreso.Models.Staff;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class PresenterHome implements IPresenterHome, IListenerResponseBussinessLogicHome {

    private BussinessLogicHome bussinessLogicHome;
    private IViewHome iViewHome;

    public PresenterHome(IViewHome iViewHome) {
        this.iViewHome = iViewHome;
        this.bussinessLogicHome = new BussinessLogicHome(PresenterHome.this);
    }

    @Override
    public void validateRecordsDataBase() {
        bussinessLogicHome.validateRecordsDataBase();
    }

    @Override
    public void getStaffs() {
        bussinessLogicHome.getStaffs();
    }

    @Override
    public void onDataBaseHasRecords(List<Staff> staffList) {
        iViewHome.onDataBaseHasRecords(staffList);
    }

    @Override
    public void onDataBaseDontHasRecords() {
        iViewHome.onDataBaseDontHasRecords();
    }

    @Override
    public void onDeviceDontHaveNetworkConnection(String message) {
        iViewHome.onDeviceDontHaveNetworkConnection(message);
    }

    @Override
    public void onApiGetUsersReponseSuccess(List<Staff> staffList) {
        iViewHome.onApiGetUsersReponseSuccess(staffList);
    }

    @Override
    public void onApiGetUsersReponseError(String message) {
        iViewHome.onApiGetUsersReponseError(message);
    }
}
