package co.com.ceiba.mobile.pruebadeingreso.HelpersView;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import co.com.ceiba.mobile.pruebadeingreso.Models.Staff;
import co.com.ceiba.mobile.pruebadeingreso.R;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class AdapterStaff extends RecyclerView.Adapter<AdapterStaff.ViewHolder> {

    public OnItemClickListener onItemClickListener;
    private List<Staff> staffList;
    private List<Staff> tempListStaffs;

    public AdapterStaff(List<Staff> staffList, OnItemClickListener onItemClickListener) {
        this.staffList = staffList;
        this.tempListStaffs = new ArrayList<>();
        this.onItemClickListener = onItemClickListener;
    }

    public void updateData(List<Staff> viewModels) {
        staffList.clear();
        staffList.addAll(viewModels);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);

        return viewHolder;
    }

    public void removeAll() {
        staffList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ViewHolder viewHolder = holder;
        viewHolder.textViewName.setText(staffList.get(position).getName());
        viewHolder.textViewPhone.setText(staffList.get(position).getPhone());
        viewHolder.textViewEmail.setText(staffList.get(position).getEmail());

        addListenerItem(viewHolder, viewHolder.getAdapterPosition());
    }

    private void addListenerItem(final ViewHolder viewHolder, final int position) {
        viewHolder.contentCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    rippleEffectBackground(viewHolder);
                }
                onItemClickListener.onItemClick(staffList.get(position));
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void rippleEffectBackground(ViewHolder viewHolder) {
        viewHolder.contentCardView.setBackgroundResource(R.drawable.ripple_efect_selected);
    }

    public List<Staff> filter(String charTextSequence) {
        if (tempListStaffs != null) {
            tempListStaffs.clear();
        }
        charTextSequence = charTextSequence.toLowerCase(Locale.getDefault());
        for (Staff staff : staffList) {
            if (staff.getName().toLowerCase(Locale.getDefault()).contains(charTextSequence)) {
                tempListStaffs.add(staff);
            }
        }
        return tempListStaffs;

    }

    @Override
    public int getItemCount() {
        return staffList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Staff item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewPhone;
        TextView textViewEmail;
        LinearLayout contentCardView;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.name);
            textViewPhone = itemView.findViewById(R.id.phone);
            textViewEmail = itemView.findViewById(R.id.email);
            contentCardView = itemView.findViewById(R.id.contentCard);

        }

    }

}
