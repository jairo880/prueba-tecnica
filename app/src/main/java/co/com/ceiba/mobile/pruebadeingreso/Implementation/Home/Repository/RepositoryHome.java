package co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Repository;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.com.ceiba.mobile.pruebadeingreso.App.App;
import co.com.ceiba.mobile.pruebadeingreso.BuildConfig;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces.IListenerResponseRepositoryHome;
import co.com.ceiba.mobile.pruebadeingreso.Models.Staff;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.SQLITE.SQLiteHelperAppDB;
import co.com.ceiba.mobile.pruebadeingreso.Utils.Constants;
import co.com.ceiba.mobile.pruebadeingreso.Volley.VolleyQueueManager;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class RepositoryHome {

    public void validateRecordsDataBase(IListenerResponseRepositoryHome handler) {
        List<Staff> staffList = new ArrayList<>();
        SQLiteHelperAppDB sqLiteHelperAppDB = new SQLiteHelperAppDB(App.getInstance().getContext());
        if (sqLiteHelperAppDB.getAllStaff().size() > 0) {
            staffList = sqLiteHelperAppDB.getAllStaff();
            handler.onDataBaseHasRecords(staffList);
        } else {
            handler.onDataBaseDontHasRecords();
        }
        sqLiteHelperAppDB.close();
    }

    public void saveStaffsLocalData(List<Staff> staffList) {
        SQLiteHelperAppDB sqLiteHelperAppDB = new SQLiteHelperAppDB(App.getInstance().getContext());
        for (Staff staff : staffList){
            sqLiteHelperAppDB.AddStaff(staff);
        }
        sqLiteHelperAppDB.close();
    }

    public void callServiceGetStaffs(final IListenerResponseRepositoryHome handler) {
        String url = BuildConfig.URL_API_BASE + Constants.GET_USERS;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Type staffType = new TypeToken<List<Staff>>() {
                        }.getType();
                        List<Staff> staff = new Gson().fromJson(String.valueOf(response), staffType);
                        handler.onApiGetUsersReponseSuccess(staff);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handler.onApiGetUsersReponseError(App.getInstance().getContext().getString(R.string.message_get_users_response_error));
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleyQueueManager.getInstance(App.getInstance().getContext()).addToRequestQueue(request);

    }

}
