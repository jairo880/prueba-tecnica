package co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.Models.Staff;

public interface IListenerResponseRepositoryHome {

    void onDataBaseHasRecords(List<Staff> staffList);

    void onDataBaseDontHasRecords();

    void onApiGetUsersReponseSuccess(List<Staff> staffList);

    void onApiGetUsersReponseError(String message);
}
