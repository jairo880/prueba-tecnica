package co.com.ceiba.mobile.pruebadeingreso.SQLITE;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.Models.Staff;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class SQLiteHelperAppDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "pruebaIngresoAppDB";
    private static final String TABLE_STAFF = "Staff";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_WEBSITE = "website";
    private static final String KEY_COMPANY = "company";

    public SQLiteHelperAppDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_STAFF_TABLE = "CREATE TABLE " + TABLE_STAFF
                + " ( "
                + KEY_ID + " TEXT ,"
                + KEY_NAME + " TEXT, "
                + KEY_USERNAME + " TEXT, "
                + KEY_EMAIL + " TEXT, "
                + KEY_ADDRESS + " TEXT, "
                + KEY_PHONE + " TEXT, "
                + KEY_WEBSITE + " TEXT, "
                + KEY_COMPANY + " TEXT "
                + " ) ";

        sqLiteDatabase.execSQL(CREATE_STAFF_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Staff");
        this.onCreate(sqLiteDatabase);
    }

    public void AddStaff(Staff staff) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, staff.getId());
        values.put(KEY_NAME, staff.getName());
        values.put(KEY_USERNAME, staff.getUsername());
        values.put(KEY_EMAIL, staff.getEmail());
        values.put(KEY_ADDRESS, new Gson().toJson(staff.getAddress()));
        values.put(KEY_PHONE, staff.getPhone());
        values.put(KEY_WEBSITE, staff.getWebsite());
        values.put(KEY_COMPANY, new Gson().toJson(staff.getCompany()));

        db.insert(TABLE_STAFF,
                null,
                values);

        db.close();
    }

    public List<Staff> getAllStaff() {
        List<Staff> allStaff = new LinkedList<Staff>();

        String query = "SELECT * FROM " + TABLE_STAFF;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Staff staff = new Staff(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7));

                allStaff.add(staff);
            } while (cursor.moveToNext());
        }

        return allStaff;
    }

    public List<Staff> getStaffsByName(int name) {
        SQLiteDatabase db = this.getReadableDatabase();

        List<Staff> allStaff = new LinkedList<Staff>();

        String query = "SELECT * FROM " + TABLE_STAFF;
        Cursor cursor = db.rawQuery("select * from " + TABLE_STAFF + "where " + KEY_NAME + "= " + name + "", null);

        if (cursor.moveToFirst()) {
            do {
                Staff staff = new Staff(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7));

                allStaff.add(staff);
            } while (cursor.moveToNext());
        }

        return allStaff;
    }

}
