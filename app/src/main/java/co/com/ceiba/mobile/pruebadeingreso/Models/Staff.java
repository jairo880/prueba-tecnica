package co.com.ceiba.mobile.pruebadeingreso.Models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class Staff implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("company")
    @Expose
    private Company company;

    public Staff(Integer id, String name, String username, String email, Address address, String phone, String website, Company company) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.website = website;
        this.company = company;
    }

    public Staff(String id, String name, String username, String email, String address, String phone, String website, String company) {
        this.id = Integer.valueOf(id);
        this.name = name;
        this.username = username;
        this.email = email;
        this.address = new Gson().fromJson(address, Address.class);
        this.phone = phone;
        this.website = website;
        this.company = new Gson().fromJson(company, Company.class);
    }

//    public Staff(String id, String name, String username, String email, String address) {
//        this.id = Integer.valueOf(id);
//        this.name = name;
//        this.username = username;
//        this.email = email;
//        this.address = new Gson().fromJson(address, Address.class);
//    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public Address getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public Company getCompany() {
        return company;
    }
}