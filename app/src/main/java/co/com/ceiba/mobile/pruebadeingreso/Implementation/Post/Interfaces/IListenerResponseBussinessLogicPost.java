package co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Interfaces;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.Models.Post;

public interface IListenerResponseBussinessLogicPost {

    void onDeviceDontHaveNetworkConnection(String message);

    void onApiGetPostUsersReponseSuccess(List<Post> postList);

    void onApiGetPostUsersReponseError(String message);

}
