package co.com.ceiba.mobile.pruebadeingreso.Implementation.Home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.io.Serializable;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.App.App;
import co.com.ceiba.mobile.pruebadeingreso.Base.BaseActivity;
import co.com.ceiba.mobile.pruebadeingreso.Base.Interfaces.IListenerStatusNetworkConnection;
import co.com.ceiba.mobile.pruebadeingreso.HelpersView.AdapterStaff;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces.IPresenterHome;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces.IViewHome;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Presenter.PresenterHome;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.PostActivity;
import co.com.ceiba.mobile.pruebadeingreso.Models.Staff;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.Utils.ServiceNetwork;
import co.com.ceiba.mobile.pruebadeingreso.Utils.Utils;

public class HomeActivity extends BaseActivity implements IViewHome,
        IListenerStatusNetworkConnection, AdapterStaff.OnItemClickListener {

    private Snackbar snackbarNetworkConnection;
    private IPresenterHome iPresenterHome;
    private RecyclerView recyclerView;
    private AdapterStaff adapterStaff;
    private LinearLayout contentProgressBar;
    private EditText editTextSearch;
    private List<Staff> staffList;
    private List<Staff> tempListStaffFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        App.getInstance().setContext(HomeActivity.this);
        implementIListenerNetworkStatus(HomeActivity.this);

        iPresenterHome = new PresenterHome(HomeActivity.this);
        recyclerView = findViewById(R.id.recyclerViewSearchResults);
        contentProgressBar = findViewById(R.id.content_progress_bar);
        editTextSearch = findViewById(R.id.editTextSearch);

        validateRecordsDataBase();
        addListeners();
    }

    private void addListeners() {
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (staffList == null) {
                    showExpandableAlert(getString(R.string.message_dont_has_user_list), R.color.colorOrange);
                    return;
                }

                if (editable.toString().isEmpty())
                    fillInformationStaffs(staffList);

                if (!editable.toString().trim().equals(""))
                    filter(editable.toString().trim());
            }
        });
    }

    private void fillInformationStaffs(List<Staff> staffList) {
        adapterStaff = new AdapterStaff(staffList, HomeActivity.this);
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(HomeActivity.this, R.anim.layout_animation_from_bottom));
        recyclerView.setAdapter(adapterStaff);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.scheduleLayoutAnimation();
    }

    private void showSnackBarMessageConnectionNetwork() {
        snackbarNetworkConnection = Snackbar.make(getWindow().getDecorView().getRootView(), getString(R.string.message_network_connection), Snackbar.LENGTH_INDEFINITE);
        snackbarNetworkConnection.show();
    }

    private void hideSnackBarMessageConnectionNetwork() {
        if (snackbarNetworkConnection == null)
            return;

        if (snackbarNetworkConnection.isShown())
            snackbarNetworkConnection.dismiss();
    }

    private void validateRecordsDataBase() {
        iPresenterHome.validateRecordsDataBase();
    }

    private void getStaffs() {
        showProgressBar();
        iPresenterHome.getStaffs();
    }

    private void showProgressBar() {
        contentProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        contentProgressBar.setVisibility(View.GONE);
    }

    private void filter(String newTextSearchFilter) {

        if (Utils.validateStringIsValid(newTextSearchFilter)) {
            this.showExpandableAlert(getString(R.string.message_invalid_name), R.color.colorOrange);
            return;
        }

        this.tempListStaffFilter = adapterStaff.filter(newTextSearchFilter);

        if (tempListStaffFilter.size() == 0) {
            this.showExpandableAlert(getString(R.string.message_staff_by_name_not_found), R.color.colorOrange);
        }

        fillInformationStaffs(tempListStaffFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        App.getInstance().setContext(HomeActivity.this);
        super.onResume();
    }

    @Override
    public void onDeviceHasNetworkConecction() {
        hideSnackBarMessageConnectionNetwork();

        if (staffList == null)
            getStaffs();
    }

    @Override
    public void onDeviceDontHasNetworkConecction() {
        showSnackBarMessageConnectionNetwork();
    }

    @Override
    public void onDataBaseHasRecords(List<Staff> staffList) {
        this.staffList = staffList;
        fillInformationStaffs(staffList);
    }

    @Override
    public void onDataBaseDontHasRecords() {
        getStaffs();
    }

    @Override
    public void onDeviceDontHaveNetworkConnection(String message) {
        hideProgressBar();
        showExpandableAlert(message, R.color.colorRed);
    }

    @Override
    public void onApiGetUsersReponseSuccess(List<Staff> staffList) {
        this.staffList = staffList;
        hideProgressBar();
        fillInformationStaffs(staffList);
    }

    @Override
    public void onApiGetUsersReponseError(String message) {
        hideProgressBar();
        showExpandableAlert(message, R.color.colorRed);
    }

    @Override
    public void onItemClick(Staff staff) {
        hideInputManager();
        if (ServiceNetwork.checkNetworkConnection(HomeActivity.this)) {
            startActivity(new Intent(HomeActivity.this, PostActivity.class)
                    .putExtra("staff", (Serializable) staff));
        } else {
            showExpandableAlert(getString(R.string.message_error_connection_network), R.color.colorOrange);
        }
    }
}