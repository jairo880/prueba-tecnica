package co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Presenter;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.BussinessLogic.BussinessLogicPost;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Interfaces.IListenerResponseBussinessLogicPost;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Interfaces.IPresenterPost;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Interfaces.IViewPost;
import co.com.ceiba.mobile.pruebadeingreso.Models.Post;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class PresenterPost implements IPresenterPost, IListenerResponseBussinessLogicPost {

    private IViewPost iViewPost;
    private BussinessLogicPost bussinessLogicPost;

    public PresenterPost(IViewPost iViewPost) {
        this.iViewPost = iViewPost;
        this.bussinessLogicPost = new BussinessLogicPost(PresenterPost.this);
    }


    @Override
    public void getPostUser(Integer id) {
        bussinessLogicPost.getPostUser(id);
    }

    @Override
    public void onDeviceDontHaveNetworkConnection(String message) {
        iViewPost.onDeviceDontHaveNetworkConnection(message);
    }

    @Override
    public void onApiGetPostUsersReponseSuccess(List<Post> postList) {
        iViewPost.onApiGetPostUsersReponseSuccess(postList);
    }

    @Override
    public void onApiGetPostUsersReponseError(String message) {
        iViewPost.onApiGetPostUsersReponseError(message);
    }
}
