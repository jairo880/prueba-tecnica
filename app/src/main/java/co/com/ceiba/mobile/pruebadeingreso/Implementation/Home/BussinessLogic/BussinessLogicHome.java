package co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.BussinessLogic;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.App.App;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces.IListenerResponseBussinessLogicHome;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces.IListenerResponseRepositoryHome;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Repository.RepositoryHome;
import co.com.ceiba.mobile.pruebadeingreso.Models.Staff;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.Utils.ServiceNetwork;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class BussinessLogicHome implements IListenerResponseRepositoryHome {

    private IListenerResponseBussinessLogicHome iListenerResponseBussinessLogicHome;
    private RepositoryHome repositoryHome;

    public BussinessLogicHome(IListenerResponseBussinessLogicHome iListenerResponseBussinessLogicHome) {
        this.iListenerResponseBussinessLogicHome = iListenerResponseBussinessLogicHome;
        this.repositoryHome = new RepositoryHome();
    }

    public void validateRecordsDataBase(){
        repositoryHome.validateRecordsDataBase(BussinessLogicHome.this);
    }


    public void getStaffs() {
        if (ServiceNetwork.checkNetworkConnection(App.getInstance().getContext())) {
            repositoryHome.callServiceGetStaffs(BussinessLogicHome.this);
        } else {
            iListenerResponseBussinessLogicHome.onDeviceDontHaveNetworkConnection(App.getInstance().getString(R.string.message_error_connection_network));
        }
    }

    @Override
    public void onDataBaseHasRecords(List<Staff> staffList) {
        iListenerResponseBussinessLogicHome.onDataBaseHasRecords(staffList);
    }

    @Override
    public void onDataBaseDontHasRecords() {
        iListenerResponseBussinessLogicHome.onDataBaseDontHasRecords();
    }

    @Override
    public void onApiGetUsersReponseSuccess(List<Staff> staffList) {

        if (staffList.size() > 0){
            repositoryHome.saveStaffsLocalData(staffList);
        }

        iListenerResponseBussinessLogicHome.onApiGetUsersReponseSuccess(staffList);
    }

    @Override
    public void onApiGetUsersReponseError(String message) {
        iListenerResponseBussinessLogicHome.onApiGetUsersReponseError(message);
    }
}
