package co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces;

public interface IPresenterHome {

    void validateRecordsDataBase();

    void getStaffs();

}
