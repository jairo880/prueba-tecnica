package co.com.ceiba.mobile.pruebadeingreso.HelpersView;


import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.Models.Post;
import co.com.ceiba.mobile.pruebadeingreso.R;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class AdapterPost extends RecyclerView.Adapter<AdapterPost.ViewHolder> {

    public OnItemClickListener onItemClickListener;
    private List<Post> postList;
    private List<Post> tempListPost;

    public AdapterPost(List<Post> postList, OnItemClickListener onItemClickListener) {
        this.postList = postList;
        this.tempListPost = new ArrayList<>();
        this.onItemClickListener = onItemClickListener;
    }

    public void updateData(List<Post> viewModels) {
        postList.clear();
        postList.addAll(viewModels);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);

        return viewHolder;
    }

    public void removeAll() {
        postList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ViewHolder viewHolder = holder;

        Post post = postList.get(position);

        if (post.getTitle() != null)
            viewHolder.textViewTittle.setText(post.getTitle());

        if (post.getBody() != null)
            viewHolder.textViewBody.setText(post.getBody());

        addListenerItem(viewHolder, viewHolder.getAdapterPosition());
    }

    private void addListenerItem(final ViewHolder viewHolder, final int position) {
        viewHolder.contentCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    rippleEffectBackground(viewHolder);
                }
                onItemClickListener.onItemClick(postList.get(position));
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void rippleEffectBackground(ViewHolder viewHolder) {
        viewHolder.contentCardView.setBackgroundResource(R.drawable.ripple_efect_selected);
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Post item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTittle;
        TextView textViewBody;
        LinearLayout contentCardView;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewTittle = itemView.findViewById(R.id.title);
            textViewBody = itemView.findViewById(R.id.body);
            contentCardView = itemView.findViewById(R.id.contentCard);
        }

    }

}

