package co.com.ceiba.mobile.pruebadeingreso.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jhonjairoduque on 21/02/2019.
 */

public class Geolocalization implements Serializable {

    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

}
