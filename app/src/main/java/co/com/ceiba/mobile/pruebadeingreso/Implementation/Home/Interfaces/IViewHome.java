package co.com.ceiba.mobile.pruebadeingreso.Implementation.Home.Interfaces;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.Models.Staff;

public interface IViewHome {

    void onDataBaseHasRecords(List<Staff> staffList);

    void onDataBaseDontHasRecords();

    void onDeviceDontHaveNetworkConnection(String message);

    void onApiGetUsersReponseSuccess(List<Staff> staffList);

    void onApiGetUsersReponseError(String message);

}
