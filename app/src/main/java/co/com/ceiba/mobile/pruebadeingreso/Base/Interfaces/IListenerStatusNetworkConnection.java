package co.com.ceiba.mobile.pruebadeingreso.Base.Interfaces;

public interface IListenerStatusNetworkConnection {

    void onDeviceHasNetworkConecction();

    void onDeviceDontHasNetworkConecction();


}
