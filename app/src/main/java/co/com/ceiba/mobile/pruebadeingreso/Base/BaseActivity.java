package co.com.ceiba.mobile.pruebadeingreso.Base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.tapadoo.alerter.Alerter;

import co.com.ceiba.mobile.pruebadeingreso.App.App;
import co.com.ceiba.mobile.pruebadeingreso.Base.Interfaces.IListenerStatusNetworkConnection;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.Utils.ServiceNetwork;

/**
 * Created by jhonjairoduque on 27/09/2017.
 */

public class BaseActivity extends AppCompatActivity {

    private BroadcastReceiver broadcastReceiver;
    private IListenerStatusNetworkConnection iListenerStatusNetworkConnection;

    public void implementIListenerNetworkStatus(IListenerStatusNetworkConnection iListenerStatusNetworkConnection) {
        this.iListenerStatusNetworkConnection = iListenerStatusNetworkConnection;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void registerBroadcastNetwork() {
        registerReceiver(this.broadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (iListenerStatusNetworkConnection != null) {
                    if (ServiceNetwork.checkNetworkConnection(App.getInstance().getContext())) {
                        iListenerStatusNetworkConnection.onDeviceHasNetworkConecction();
                    } else {
                        iListenerStatusNetworkConnection.onDeviceDontHasNetworkConecction();
                    }
                }
            }
        }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onStop() {
        if (broadcastReceiver != null)
            unregisterReceiver(broadcastReceiver);
        super.onStop();
    }

    protected void showExpandableAlert(String information, int color) {
        Alerter.create(BaseActivity.this)
                .setText(information)
                .setDuration(3000)
                .setBackgroundColorRes(color)
                .setIcon(R.drawable.notification)
                .enableSwipeToDismiss()
                .show();
    }

    public void hideInputManager() {
        ((InputMethodManager) BaseActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerBroadcastNetwork();
    }

}