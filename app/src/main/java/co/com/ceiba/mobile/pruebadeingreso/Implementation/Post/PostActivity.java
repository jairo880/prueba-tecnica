package co.com.ceiba.mobile.pruebadeingreso.Implementation.Post;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.App.App;
import co.com.ceiba.mobile.pruebadeingreso.Base.BaseActivity;
import co.com.ceiba.mobile.pruebadeingreso.Base.Interfaces.IListenerStatusNetworkConnection;
import co.com.ceiba.mobile.pruebadeingreso.HelpersView.AdapterPost;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Interfaces.IPresenterPost;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Interfaces.IViewPost;
import co.com.ceiba.mobile.pruebadeingreso.Implementation.Post.Presenter.PresenterPost;
import co.com.ceiba.mobile.pruebadeingreso.Models.Post;
import co.com.ceiba.mobile.pruebadeingreso.Models.Staff;
import co.com.ceiba.mobile.pruebadeingreso.R;

public class PostActivity extends BaseActivity implements IViewPost, IListenerStatusNetworkConnection,
        AdapterPost.OnItemClickListener {

    private Snackbar snackbarNetworkConnection;
    private Staff currentStaff;
    private TextView textViewName;
    private TextView textViewPhone;
    private TextView textViewEmail;
    private RecyclerView recyclerView;
    private IPresenterPost iPresenterPost;
    private AdapterPost adapterPost;
    private Toolbar toolbar;
    private LinearLayout contentProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        App.getInstance().setContext(PostActivity.this);
        implementIListenerNetworkStatus(PostActivity.this);

        iPresenterPost = new PresenterPost(PostActivity.this);
        toolbar = findViewById(R.id.toolbar_post);
        textViewName = findViewById(R.id.name);
        textViewPhone = findViewById(R.id.phone);
        textViewEmail = findViewById(R.id.email);
        recyclerView = findViewById(R.id.recyclerViewPostsResults);
        contentProgressBar = findViewById(R.id.content_progress_bar);

        settingsActionBar();
        getStaff();
        fillInformationStaff();
    }

    private void settingsActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.arrowbackiconwhite));

    }

    private void getStaff() {
        if (getIntent().getExtras() == null)
            return;

        this.currentStaff = (Staff) getIntent().getExtras().getSerializable("staff");
    }

    private void getPostUser(Integer id) {
        showProgressBar();
        iPresenterPost.getPostUser(id);
    }

    private void fillInformationStaff() {
        if (currentStaff.getName() != null)
            textViewName.setText(currentStaff.getName());

        if (currentStaff.getPhone() != null)
            textViewPhone.setText(currentStaff.getPhone());

        if (currentStaff.getEmail() != null)
            textViewEmail.setText(currentStaff.getEmail());
    }

    private void fillInformationPostStaff(List<Post> postList) {
        adapterPost = new AdapterPost(postList, PostActivity.this);
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(PostActivity.this, R.anim.layout_animation_from_bottom));
        recyclerView.setAdapter(adapterPost);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.scheduleLayoutAnimation();
    }

    private void showSnackBarMessageConnectionNetwork() {
        snackbarNetworkConnection = Snackbar.make(getWindow().getDecorView().getRootView(), getString(R.string.message_network_connection), Snackbar.LENGTH_INDEFINITE);
        snackbarNetworkConnection.show();
    }

    private void hideSnackBarMessageConnectionNetwork() {
        if (snackbarNetworkConnection == null)
            return;

        if (snackbarNetworkConnection.isShown())
            snackbarNetworkConnection.dismiss();
    }

    private void showProgressBar(){
        contentProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        contentProgressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        App.getInstance().setContext(PostActivity.this);
        super.onResume();
    }

    @Override
    public void onDeviceHasNetworkConecction() {
        hideSnackBarMessageConnectionNetwork();
        getPostUser(currentStaff.getId());
    }

    @Override
    public void onDeviceDontHasNetworkConecction() {
        showSnackBarMessageConnectionNetwork();
    }

    @Override
    public void onDeviceDontHaveNetworkConnection(String message) {
        hideProgressBar();
        showExpandableAlert(message, R.color.colorRed);
    }

    @Override
    public void onApiGetPostUsersReponseSuccess(List<Post> postList) {
        hideProgressBar();
        fillInformationPostStaff(postList);
    }

    @Override
    public void onApiGetPostUsersReponseError(String message) {
        hideProgressBar();
        showExpandableAlert(message, R.color.colorRed);
    }

    @Override
    public void onItemClick(Post item) {
    }
}
